#!/bin/bash

#SBATCH --exclusive

function error() {
    echo "Error: $@" >&2
}

function assert_file_exists() {
    if [ ! -f "$1" ]; then
        error "file not found: $1"
        exit 1
    fi
}

# Command line parameters
dataset=${1:?No dataset specified}
samples=${2:?No samples specified}
name=${3:?No name specified}

# Local variables
nodes=$SLURM_JOB_NUM_NODES
testfile="tests/${dataset}-${nodes}s.par"
image="${IMG_DIR}/ompc.sif"

# Sanitization
assert_file_exists "$testfile"
assert_file_exists "$image"

# Environment variables
export AWAVE_MAX_PARALLEL_SHOT="$(( nodes-1 ))"
export OMP_NUM_THREADS=24
export OMPCLUSTER_ENABLE_PACKING=0
export OMPCLUSTER_FT_DISABLE=1
export OMPCLUSTER_NUM_EVENT_HANDLERS=24
export OMPCLUSTER_NUM_EVENT_COMM=24
export MPIR_CVAR_CH4_NUM_VCIS=24

# Benchmark
for i in $(seq 1 $samples); do
    /usr/bin/time -f 'Elapsed: %E' \
        srun --mpi=pmi2 -n $SLURM_JOB_NUM_NODES \
        singularity exec "$image" \
        ./build/awave2d-ompcluster -p "$testfile"
done
