#!/bin/bash

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

name="awave"

datasets=(marm2 sigsb)
nodes=(2 3 5 9 17)
samples=10

# Local variables
count=0
logdir="results/$name"

# Where logs will be saved
if [ ! -d "$logdir" ]; then
    mkdir -p "$logdir"
fi

echo "==> LAUNCHING JOBS ($name)"

for dataset in ${datasets[@]}; do
    for node in ${nodes[@]}; do
        # Unique identifier of this job
        id="${dataset}-${node}n"
        # Lauch awave job that will collect 5 samples
        sbatch -N "$node" \
               -J "$id" \
               -o "$logdir/$id.out" \
               --time=12:00:00 \
               --account $ACCOUNT \
               --partition $PARTITION \
               --time $TIMELIMIT \
            ${script_dir}/job.sh $dataset $samples $name
        # Increment job counter
        ((count++))
    done
done

echo "==> FINISHED! $count JOBS LAUNCHED."
