#!/usr/bin/env python3

import os
import sys
import glob
import pprint

def to_seconds(timestr):
    minutes, rest = timestr.split(":")
    return float(minutes) * 60 + float(rest)


def main():
    if len(sys.argv) < 2:
        print("Error: expected a directory with log files.", file=sys.stderr)
        sys.exit(1)

    data = []
    fnames = glob.glob(f"{sys.argv[1]}/*.out")

    # For each output log file, extract the elapsed time samples
    for fname in fnames:
        with open(fname, 'r') as f:
            basename = os.path.splitext(os.path.basename(fname))[0]
            dataset = basename.split("-")[0]
            nodes = basename.split("-")[1][:-1]

            samples = []
            for line in f.readlines():
                if line.startswith("Elapsed: "):
                    samples.append(to_seconds(line.split(" ")[1].strip()))

            data.append({
                'dataset': dataset,
                'nodes': int(nodes),
                'samples': samples
            })

    header = "# data  nodes"
    for i in range(len(data[0]['samples'])):
        header += f"  sample{i}"

    lines = []

    for record in data:
        dataset = record['dataset']
        nodes = record['nodes']
        samples = [f"{sample:7.2f}" for sample in record['samples']]
        lines.append(f"{dataset:>6}  {nodes:5}  {'  '.join(samples)}")

    lines = [header] + sorted(lines)
    print("\n".join(lines))

if __name__ == '__main__':
    main()
