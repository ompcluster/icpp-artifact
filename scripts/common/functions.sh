#!/bin/bash

# ------------------------------------------------------------------------------
# Library of Reusable Functions
# ------------------------------------------------------------------------------
# This script is not meant to be called directly, but rather invoked by a
# OMPCBench launcher script.
#
# Authors:
#   OMPC team
# ------------------------------------------------------------------------------

# Gather samples by running command passed as string
function run() {
    # Change directory to application root
    cd $(realpath $OMPCBENCH_APPLICATION_DIR)

    # Create new file streams that replicate stdout and stderr
    exec 3>&1 4>&2

    # For each sample
    for sample in $(seq 1 $samples); do

        # --------------------------------------------------------------------------
        set +e # Disable quit on failure

        # Run command passed as string and measure the elapsed (real) time
        # This command will ensure the output of eval goes to stdout/stderr
        # while keeping the output of the `time` built-in into a variable.
        elapsed=$(TIMEFORMAT="%R"; { time eval "$1" 1>&3 2>&4 ; } 2>&1)

        # Grab the return code from the command above
        code="$?"

        set -e # Enable quit on failure
        # --------------------------------------------------------------------------

        # Save this failure to the log
        if [[ "$code" != 0 ]]; then
            echo "$SLURM_JOB_ID,$sample,$code" >>$failurelog
        fi

        echo
        echo "==> Executed sample $sample in $elapsed seconds."
        echo

    done

    # Close file streams
    exec 3>&- 4>&-
}

function error() {
    echo "Error: $@" >&2
}

function warn() {
    echo "Warning: $@"
}

function hline() {
    echo "=================================================="
}
