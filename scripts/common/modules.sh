#!/bin/bash

# ------------------------------------------------------------------------------
# Module Loading Logic
# ------------------------------------------------------------------------------
# This script is not meant to be called directly, but rather invoked by a
# OMPCBench launcher script.
#
# Authors:
#   OMPC team
# ------------------------------------------------------------------------------

if [[ "$OMPCBENCH_MODULE_PURGE" -eq 1 ]]; then
    module purge
fi

for module in $OMPCBENCH_MODULES; do
    module load $module
done
