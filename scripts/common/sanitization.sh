#!/bin/bash

# ------------------------------------------------------------------------------
# Shared Sanitization Checks
# ------------------------------------------------------------------------------
# This script is not meant to be called directly, but rather invoked by a
# OMPCBench launcher script.
#
# Authors:
#   OMPC team
# ------------------------------------------------------------------------------

# SANITIZATION {{{1

if [[ ! -v OMPCBENCH_RESULT_DIR ]]; then
    error "OMPCBENCH_RESULT_DIR variable is not set!"
fi

if [[ ! -v OMPCBENCH_APPLICATION_CLI ]]; then
    error "OMPCBENCH_APPLICATION_CLI variable is not set!"
fi

if [[ ! -v OMPCBENCH_APPLICATION_DIR ]]; then
    error "OMPCBENCH_APPLICATION_DIR variable is not set!"
fi

if [[ ! -v OMPCBENCH_APPLICATION_BIN ]]; then
    error "OMPCBENCH_APPLICATION_BIN variable is not set!"
fi

if [[ ! -d "$OMPCBENCH_RESULT_DIR" ]]; then
    error "result directory does not exist!"
    error "- Path: $OMPCBENCH_RESULT_DIR"
    exit 3
fi

if [[ ! -d "$OMPCBENCH_APPLICATION_DIR" ]]; then
    error "Application directory does not exist!"
    exit 4
fi

if [[ ! -x "$OMPCBENCH_APPLICATION_DIR/$OMPCBENCH_APPLICATION_BIN" ]]; then
    error "Application '$OMPCBENCH_APPLICATION_DIR/$OMPCBENCH_APPLICATION_BIN' is not executable!"
    error "maybe you forgot do compile it?"
    exit 5
fi

# Warnings ---------------------------------------------------------------------

if [[ -z "$OMPCBENCH_IMAGE" ]]; then
    warn "not using container!"
fi
