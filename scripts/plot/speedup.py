#!/usr/bin/env python3

import click
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy as np

# Default dependencies to display
DEFAULT_DEPS = ["trivial", "fft", "stencil_1d_periodic", "tree"]


def plot_speedup(df, *, confidence_level=95):
    """Crate plot and adjust its style."""
    # Create bar graph (categorial plot, kind = bar) with Seaborn
    # See: http://seaborn.pydata.org/generated/seaborn.catplot.html
    # TODO: use speedup instead of time
    g = sns.catplot(
        data=df,
        kind="bar",
        x="nodes",
        y="time",
        hue="dependency",
        col="CCR",
        ci=confidence_level,
        n_boot=1000,
    )

    # Formatting
    g.despine(left=True)
    g.set_axis_labels("Worker Nodes (#)", "Wall Time (ms)")
    g.legend.set_title("")

    return g


# fmt: off
@click.command()
@click.argument("PICKLE", nargs=1)
@click.option("-o", "--output", default=None, type=click.File("w"),
    help="Output image file.")
@click.option("-d", "--deps", default=None, type=str,
   help="Comma-separated list of dependencies to display.")
@click.option("-c", "--confidence", "confidence", default=95, type=float,
   help="Confidence level of the bootstrap interval.")
# fmt: on
def main(pickle, output, deps, confidence):
    """Create a speedup graph from a Pickle file.

    The Pickle file must be exported with `ompcbench analyze --raw` command.
    Feel free to adapt the script source to your needs."""

    # Initialization
    # -----------------------------------------------------

    # Read input data
    df_original = pd.read_pickle(pickle)
    # Create a working copy
    df = df_original.copy()

    # Seaborn style
    sns.set_theme(style="whitegrid")
    sns.set_context("paper")
    sns.set(font="Fira Sans Condensed")

    # Validation
    # -----------------------------------------------------

    # Select which dependencies to display
    all_deps = df.dependency.unique()
    if deps == "all":
        deps = all_deps
    else:
        deps = set(all_deps).intersection(deps.split(",") if deps else DEFAULT_DEPS)

    # Preparation
    # -----------------------------------------------------

    # Change the nodes column to represent the number of worker nodes
    df.nodes = df.nodes - 1
    # Change `comp to comm` -> `comm to comp` ratio
    df.CCR = 1.0 / df.CCR

    mask = (
        df.dependency.apply(lambda x: x in deps)
        # Keep only the largest workload
        & (df.iterations == df.iterations.max())
    )

    df = df[mask]

    print(df.to_string())

    # Graphing
    # -----------------------------------------------------

    plot_speedup(df, confidence_level=confidence)

    # Output
    # -----------------------------------------------------

    if output:
        plt.savefig(output.name)
    else:
        plt.show()


if __name__ == "__main__":
    main()
