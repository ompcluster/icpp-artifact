#!/usr/bin/env python3

import click
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# fmt: off
@click.command()
@click.argument('PICKLE', nargs=1)
@click.option("-o", "--output", default=None, type=click.File("w"),
              help="Output file.")
# fmt: on
def main(pickle, output):
    """Plot a stacked bar graph representing overhead."""

    # Initialization
    # ----------------------------------------------------------------

    df = pd.read_pickle(pickle)

    sns.set_theme(style="darkgrid")
    sns.set_context("paper")

    # Preparation
    # ----------------------------------------------------------------

    # fmt: off
    # Transform the `iterations` column in the index temporarily. Some
    # operations would otherwise destroy the contents of this column.
    df = df.set_index("iterations")
    # Ignore dump time from execution
    df = df.assign(wall_time=df.wall_time - df.dump).drop("dump", axis=1)
    # Calculate time spent inside event handler
    df = df.assign(
        handler_time = df.wall_time - df.startup - df.shutdown - df.schedule
    )
    # Divide everything by wall time and multiply by 100 to get %
    df = df.div(df.wall_time, axis=0) * 100
    # Remove unneded column
    df = df.drop("wall_time", axis=1)
    # Restore index with iteration count
    df = df.reset_index()
    # Change column order
    df = (
        df[["iterations", "handler_time", "startup", "shutdown", "schedule"]]
        .rename({
            "handler_time": "Event Handler",
            "startup": "Startup",
            "shutdown": "Shutdown",
            "schedule": "Scheduling",
        }, axis=1)
    )
    # fmt: on

    # Graphing
    # ----------------------------------------------------------------

    print(df)

    _, ax = plt.subplots()
    ax = df.plot(
        x="iterations",
        kind="bar",
        stacked=True,
        ax=ax,
        xlabel="Iterations per task (#)",
        ylabel="% of Execution Time",
        ylim=(0, 100),
        rot=0,  # Rotate the x labels by 0 degrees
    )

    # Output
    # ----------------------------------------------------------------

    plt.tight_layout()
    if output:
        plt.savefig(output.name)
    else:
        plt.show()


if __name__ == "__main__":
    main()
