#!/usr/bin/env python3

"""
Plot a comparison between runtimes.
"""

import sys
import os
import click
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# fmt: off
@click.command()
@click.argument("PICKLE", nargs=1)
@click.option("-o", "--output", default=None, type=click.File("w"),
              help="Output file.")
# fmt: on
def main(pickle, output):
    """Plot a comparison graph between runtimes."""

    # Initialization
    # -----------------------------------------------------

    df = pd.read_pickle(pickle)

    sns.set_theme(style="darkgrid")
    sns.set_context("paper")
    sns.set(rc={"figure.figsize": (11.7, 8.27)})

    # Preparation
    # -----------------------------------------------------

    # Remove head node from count
    df.nodes = df.nodes - 1
    # Remove unnecessary columns
    df = df.drop(["CCR", "jobid", "sample", "kernel", "iterations"], axis=1)
    # Convert column types to integer
    df = df.astype(
        {
            "nodes": "int64",
            "width": "int64",
            "steps": "int64",
        }
    )
    # Calculate the mean of the samples
    df = (
        df.groupby(["strategy", "nodes", "dependency", "width", "steps"])
        .agg("mean")
        .reset_index()
    )

    # print(
    #     df.groupby(["strategy", "nodes", "dependency", "width", "steps"])
    #     .agg("mean")
    #     .to_string()
    # )
    # sys.exit(0)

    # Calculate speedup using single worker node execution as baseline
    records = []
    unique_df = df[["strategy", "dependency", "nodes"]].copy().drop_duplicates()

    # Iterate over every unique combination of strategy, dependency, and nodes
    # in the dataset
    for _, triple in unique_df.iterrows():

        # Retrieve "serial" (one worker node) time
        M1 = (
            (df.strategy == triple.strategy)
            & (df.dependency == triple.dependency)
            & (df.nodes == 1)
            & (df.width == triple.nodes * 8)
        )

        try:
            t_serial = float(df[M1][["time"]].iloc[0])
        except IndexError as error:
            continue

        # Retrieve "parallel" (many worker node) time
        M2 = (
            (df.strategy == triple.strategy)
            & (df.dependency == triple.dependency)
            & (df.nodes == triple.nodes)
            & (df.width == triple.nodes * 8)
        )

        try:
            t_parallel = float(df[M2][["time"]].iloc[0])
        except IndexError as error:
            continue

        records.append(
            {
                "strategy": triple.strategy,
                "dependency": triple.dependency,
                "nodes": triple.nodes,
                "speedup": t_serial / t_parallel,
                "time": t_parallel,
            }
        )

    df = pd.DataFrame.from_records(records)

    # Change strategy names to make the graph readable
    # fmt: off
    df.strategy = df.strategy.map(
        lambda x: {
            "default": "OmpCluster",
            "mpi": "MPI",
            "mpi_openmp": "MPI+OpenMP"
        }[x]
    )
    # fmt: on

    print(df.to_string())

    # Graphing
    # -----------------------------------------------------

    # Create a figure with 2x2 plots (axes)
    fig, axes = plt.subplots(ncols=2, nrows=2)
    indices = [(0, 0), (0, 1), (1, 0), (1, 1)]

    # Iterate over dependencies and their respective axes
    for (dep, df), (row, col) in zip(df.groupby("dependency"), indices):
        ax = axes[row, col]

        # Iterate over strategies and plot them
        for strat, ddf in df.groupby("strategy"):
            ax.plot(ddf.nodes, ddf.time, "o-", label=strat)

        # Make it readable
        ax.set_title(dep.upper(), fontweight="bold")
        ax.set_xlabel("Worker Nodes (#)")
        ax.set_xscale("log")
        ax.set_xticks([1, 2, 4, 8, 16, 32])
        ax.set_xticklabels([1, 2, 4, 8, 16, 32])
        ax.set_ylabel("Time (s)")

    # Top level figure title
    fig.suptitle("Weak Scalability")

    axes[0, 1].legend(loc="upper left", bbox_to_anchor=(1.1, 0.9))

    # Output
    # -----------------------------------------------------

    plt.tight_layout()
    if output:
        plt.savefig(output.name)
    else:
        plt.show()


if __name__ == "__main__":
    main()
