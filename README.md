# ICPP 22 Artifact

Artifacts needed to reproduce the results of the paper *The OpenMP Cluster Programming Model* inside a computer cluster.

## Requirements

 - [Singularity v3.5](https://sylabs.io/guides/3.5/user-guide/index.html)
 - [SLURM job manager](https://slurm.schedmd.com/)
 - [Environment Modules](http://modules.sourceforge.net/)
   - MPICH v3.4.2 compiled with UCX backend and VCI enabled (`--with-ch4-max-vcis=64 --enable-thread-cs=per-vci`)
   - UCX v1.11.2
 - Python 3.9.3
 - Access to a NFS-like file system that is accessible from all cluster nodes.

## Preparation

Download the artifact tarball into a directory that can be accessed by all nodes (e.g., scratch paths).

```bash
# Go to a NFS-like/scratch directory, download the artifact, extract it and go to it.
cd icpp-artifact
```

**NOTES**: all the following commands **MUST** be executed inside the artifact folder.

First, install our python benchmark tool, OMPCBench, and download all
singularity images using the following commands:

```bash
# This command will install python package in your environment. Python
# environment management tools like virtualenv and conda are recommended.
./setup.sh
# This command will take several minutes to executed. You may want to execute it
# as a SLURM job to not overload your cluster login node.
./get_images.sh
```

Then, edit the [env.sh](./env.sh) file with:
 -  the correct name of your MPICH and UCX modules separated by spaces,
 -  the **account** and *partition** settings with the ones required by the Slurm environment of the cluster being used.

Next, source the file:

```bash
source env.sh
```

## Experiments

Finally, the environment should be ready to run the experiments

### Runtimes

First, start the runtime experiments by launching its SLURM jobs with the following command:

```bash
ompcbench run config/slurm.yaml experiments/runtimes.yaml --no-verify
```

Once all the jobs are completed, the results will be at [results](./results), use OMPC Bench to analyze the results:

```bash
ompcbench analyze results/runtimes_000/
```

### Scalability

Then, repeat the same process for the scalability experiments. Here it is needed to run one OMPC Bench command for each node amount as follows:

```bash
ompcbench run config/slurm.yaml experiments/scalability_2.yaml --no-verify
ompcbench run config/slurm.yaml experiments/scalability_4.yaml --no-verify
ompcbench run config/slurm.yaml experiments/scalability_8.yaml --no-verify
ompcbench run config/slurm.yaml experiments/scalability_16.yaml --no-verify
ompcbench run config/slurm.yaml experiments/scalability_32.yaml --no-verify
ompcbench run config/slurm.yaml experiments/scalability_64.yaml --no-verify
```

Copy all the scalability results to the same directory and then analyze it with OMPC Bench:

```bash
# Copy all scalability results to the same directory (e.g., results/scalability).
# With zsh this can be achieved with the following command:
# cp -r ./results/scalability_00* ./results/scalability
ompcbench analyze results/scalability/
```

### Awave

Finally, run the Awave experiments. Since OMPC Bench does not support Awave, run the following bash script to launch the jobs:

```bash
bash ./scripts/awave/benchmark.sh
```

Once all the jobs are completed, the results can be analyzed with the following commands:

```bash
python3 ./scripts/awave/analyze.py ./results/awave
```
