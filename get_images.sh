#!/bin/bash
set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if ! singularity --version 2>&1 | grep -e "3\.*" > /dev/null; then
    echo "Missing Singularity 3.5"
    exit 1;
fi

echo "Download container images"
mkdir -p ${SCRIPT_DIR}/sif
singularity pull -F ${SCRIPT_DIR}/sif/ompc.sif docker://registry.gitlab.com/ompcluster/icpp-artifact/runtime-dev:icpp-artifact
singularity pull -F ${SCRIPT_DIR}/sif/charm.sif docker://registry.gitlab.com/ompcluster/icpp-artifact/taskbench-charm-ucx:icpp-artifact
singularity pull -F ${SCRIPT_DIR}/sif/starpu.sif docker://registry.gitlab.com/ompcluster/icpp-artifact/taskbench-starpu:icpp-artifact
