#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export IMG_DIR="${SCRIPT_DIR}/sif"

# Modify if the module name is different
export MODULES="mpich/3.4.2-ucx"

# Modify if the Slurm configuration is different
export ACCOUNT="brcluster"
export PARTITION="ict_cpu"
export TIMELIMIT="02:00:00"
