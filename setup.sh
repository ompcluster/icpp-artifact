#!/bin/bash
set -ex

if ! python -V 2>&1 | grep "3.9.3" > /dev/null; then
    echo "Missing Python 3.9.3"
    exit 1;
fi

echo "Installing OMPC Bench with pip"
pip3 install setup/ompcbench-1.0.3-py3-none-any.whl
